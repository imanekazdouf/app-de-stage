USE [Sells]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[CltID] [int] NOT NULL,
	[NomClt] [varchar](50) NULL,
	[AdressClt] [varchar](50) NULL,
	[TelClt] [varchar](50) NULL,
	[FaxClt] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[CltID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Commande]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Commande](
	[IDCommande] [int] NOT NULL,
	[CltID] [int] NULL,
	[DateCommande] [date] NULL,
	[PrixTotal] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDCommande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetailCommande]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetailCommande](
	[IDProduit] [int] NOT NULL,
	[IDCommande] [int] NOT NULL,
	[QuantiteCmd] [int] NULL,
 CONSTRAINT [pk_cmdPrd] PRIMARY KEY CLUSTERED 
(
	[IDProduit] ASC,
	[IDCommande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Fournisseur]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fournisseur](
	[IDFournisseur] [int] NOT NULL,
	[AdressFour] [varchar](50) NULL,
	[TelFourniss] [varchar](50) NULL,
	[FaxFourn] [varchar](50) NULL,
	[SOCIETE] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDFournisseur] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mesure]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mesure](
	[nbrPoutrelle] [int] NULL,
	[MCarre] [float] NULL,
	[NPTotal] [int] NULL,
	[ML] [float] NULL,
	[NH] [float] NULL,
	[PTS] [float] NULL,
	[chambre] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produit]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produit](
	[IDProduit] [int] NOT NULL,
	[NomProduit] [varchar](50) NULL,
	[QuantiteEnStock] [int] NULL,
	[PrixUnitaire] [float] NULL,
	[IDFournisseur] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDProduit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Unite]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unite](
	[chambre] [int] IDENTITY(1,1) NOT NULL,
	[axe] [float] NULL,
	[vide] [float] NULL,
	[IDCommande] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[chambre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[utilisateur]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[utilisateur](
	[Email] [varchar](50) NOT NULL,
	[userName] [varchar](50) NULL,
	[prenomUser] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[dateNaiss] [date] NULL,
	[imageUser] [varchar](max) NULL,
	[Role] [varchar](50) NULL,
	[entreprise] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Client] ([CltID], [NomClt], [AdressClt], [TelClt], [FaxClt]) VALUES (1, N'clt11', N'address11', N'060215222', N'05230479')
INSERT [dbo].[Client] ([CltID], [NomClt], [AdressClt], [TelClt], [FaxClt]) VALUES (2, N'clt2', N'address22', N'06011148', N'05230009')
INSERT [dbo].[Client] ([CltID], [NomClt], [AdressClt], [TelClt], [FaxClt]) VALUES (3, N'Nom ', N'Adress 33', N'06545768', N'0556465465')
INSERT [dbo].[Client] ([CltID], [NomClt], [AdressClt], [TelClt], [FaxClt]) VALUES (4, N'sdgteg', N'yuo7546', N'0623547832', N'0512457823')
GO
INSERT [dbo].[Commande] ([IDCommande], [CltID], [DateCommande], [PrixTotal]) VALUES (11, 2, CAST(N'2020-05-11' AS Date), 200)
INSERT [dbo].[Commande] ([IDCommande], [CltID], [DateCommande], [PrixTotal]) VALUES (22, 1, CAST(N'2020-05-21' AS Date), 150)
INSERT [dbo].[Commande] ([IDCommande], [CltID], [DateCommande], [PrixTotal]) VALUES (33, 3, CAST(N'2020-03-18' AS Date), 500)
INSERT [dbo].[Commande] ([IDCommande], [CltID], [DateCommande], [PrixTotal]) VALUES (44, 4, CAST(N'2020-01-01' AS Date), 350)
GO
INSERT [dbo].[DetailCommande] ([IDProduit], [IDCommande], [QuantiteCmd]) VALUES (1, 33, 20)
INSERT [dbo].[DetailCommande] ([IDProduit], [IDCommande], [QuantiteCmd]) VALUES (2, 11, 15)
INSERT [dbo].[DetailCommande] ([IDProduit], [IDCommande], [QuantiteCmd]) VALUES (3, 22, 14)
GO
INSERT [dbo].[Fournisseur] ([IDFournisseur], [AdressFour], [TelFourniss], [FaxFourn], [SOCIETE]) VALUES (1, N'addfourn1', N'06127586', N'05777613', N'societe1')
INSERT [dbo].[Fournisseur] ([IDFournisseur], [AdressFour], [TelFourniss], [FaxFourn], [SOCIETE]) VALUES (2, N'addfourn2', N'06127511', N'05777122', N'societe2')
GO
INSERT [dbo].[Mesure] ([nbrPoutrelle], [MCarre], [NPTotal], [ML], [NH], [PTS], [chambre]) VALUES (12, 78, 72, 120, 120, 487, 2)
INSERT [dbo].[Mesure] ([nbrPoutrelle], [MCarre], [NPTotal], [ML], [NH], [PTS], [chambre]) VALUES (23, 50, 48, 100, 300, 1000, 3)
INSERT [dbo].[Mesure] ([nbrPoutrelle], [MCarre], [NPTotal], [ML], [NH], [PTS], [chambre]) VALUES (5, 10, 75, 546, 858, 544, 5)
GO
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (1, N'Nom Produit1', 2, 120, 1)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (2, N'Nom Produit2', 22, 1200, 1)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (3, N'produit3', 12, 10, 1)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (4, N'produit4', 10, 50, 2)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (5, N'produit5', 50, 14, 2)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (6, N'produit6', 100, 23, 2)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (7, N'Nom Produit7', 95, 14, 2)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (8, N'Nom Produit8', 547, 10, 1)
INSERT [dbo].[Produit] ([IDProduit], [NomProduit], [QuantiteEnStock], [PrixUnitaire], [IDFournisseur]) VALUES (9, N'Nom Produit 9', 154, 8, 2)
GO
SET IDENTITY_INSERT [dbo].[Unite] ON 

INSERT [dbo].[Unite] ([chambre], [axe], [vide], [IDCommande]) VALUES (2, 11, 12, 11)
INSERT [dbo].[Unite] ([chambre], [axe], [vide], [IDCommande]) VALUES (3, 10, 5, 22)
INSERT [dbo].[Unite] ([chambre], [axe], [vide], [IDCommande]) VALUES (5, 12, 13, 33)
INSERT [dbo].[Unite] ([chambre], [axe], [vide], [IDCommande]) VALUES (6, 20, 15, 44)
SET IDENTITY_INSERT [dbo].[Unite] OFF
GO
INSERT [dbo].[utilisateur] ([Email], [userName], [prenomUser], [password], [dateNaiss], [imageUser], [Role], [entreprise]) VALUES (N'Email', N'fadili', N'yassine', N'123', CAST(N'2020-09-06' AS Date), N'yassine.jpg', NULL, NULL)
INSERT [dbo].[utilisateur] ([Email], [userName], [prenomUser], [password], [dateNaiss], [imageUser], [Role], [entreprise]) VALUES (N'Email 2', N'abdellaoui', N'ahmad', N'Mot de passe', CAST(N'2020-09-06' AS Date), N'ahmad.png', NULL, NULL)
INSERT [dbo].[utilisateur] ([Email], [userName], [prenomUser], [password], [dateNaiss], [imageUser], [Role], [entreprise]) VALUES (N'Email 5', N'chahidi', N'youssra', N'xdhhgikuo', CAST(N'2020-09-14' AS Date), N'youssra.jpg', NULL, NULL)
INSERT [dbo].[utilisateur] ([Email], [userName], [prenomUser], [password], [dateNaiss], [imageUser], [Role], [entreprise]) VALUES (N'email5', N'salmi', N'morad', N'qdsfikijpyut', CAST(N'2020-09-07' AS Date), N'morad.jpg', NULL, NULL)
GO
ALTER TABLE [dbo].[Commande]  WITH CHECK ADD FOREIGN KEY([CltID])
REFERENCES [dbo].[Client] ([CltID])
GO
ALTER TABLE [dbo].[DetailCommande]  WITH CHECK ADD FOREIGN KEY([IDCommande])
REFERENCES [dbo].[Commande] ([IDCommande])
GO
ALTER TABLE [dbo].[DetailCommande]  WITH CHECK ADD  CONSTRAINT [FK__DetailCom__IDPro__29221CFB] FOREIGN KEY([IDProduit])
REFERENCES [dbo].[Produit] ([IDProduit])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DetailCommande] CHECK CONSTRAINT [FK__DetailCom__IDPro__29221CFB]
GO
ALTER TABLE [dbo].[Mesure]  WITH CHECK ADD FOREIGN KEY([chambre])
REFERENCES [dbo].[Unite] ([chambre])
GO
ALTER TABLE [dbo].[Produit]  WITH CHECK ADD FOREIGN KEY([IDFournisseur])
REFERENCES [dbo].[Fournisseur] ([IDFournisseur])
GO
ALTER TABLE [dbo].[Unite]  WITH CHECK ADD FOREIGN KEY([IDCommande])
REFERENCES [dbo].[Commande] ([IDCommande])
GO
/****** Object:  StoredProcedure [dbo].[ADD_PRODUCT]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ADD_PRODUCT]
	@ID_prd INT, @NOM_PRODUIT VARCHAR(40), @QTE_STOCK INT, @PRIX VARCHAR(25),@ID_FOUR INT
AS

INSERT INTO [Produit]
           ([IDProduit] 
           ,[NomProduit]
           ,[QuantiteEnStock]
           ,[PrixUnitaire]
		   ,[IDFournisseur]
            )
     VALUES
           (@ID_prd
           ,@NOM_PRODUIT
           ,@QTE_STOCK
           ,@PRIX
		   ,@ID_FOUR
           )
GO
/****** Object:  StoredProcedure [dbo].[AddUser]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddUser]
	@Email VARCHAR(50),@NomUser VARCHAR(30), @PrenmUser VARCHAR(30),
	 @PASSUser VARCHAR(50),@dateNaiss date,
	@imgUser  VARCHAR(MAX)
AS
INSERT INTO [utilisateur]
           (
		    [Email]
		   ,[userName]
           ,[prenomUser]
		   ,[password]
           ,[dateNaiss]
           ,[imageUser]
		   )
     VALUES
           (@Email
           ,@NomUser
		   ,@PrenmUser
		   ,@PASSUser
           ,@dateNaiss
           ,@imgUser
		   )
GO
/****** Object:  StoredProcedure [dbo].[Ajouter_CLIENTS]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Ajouter_CLIENTS]
	@ID_CLIENT INT,@NOM_CLIENT VARCHAR(30),@Adress VARCHAR(50), @TEL_CLIENT VARCHAR(12), @FAX_CLIENT VARCHAR(50)
	
AS

BEGIN
INSERT INTO [Client]
         	
			(
			[CltID],
			[NomClt],
			[AdressClt],
			[TelClt],
			[FaxClt])
     VALUES
           (@ID_CLIENT,
		    @NOM_CLIENT
		   ,@Adress
           ,@TEL_CLIENT
           ,@FAX_CLIENT
		   )
END
GO
/****** Object:  StoredProcedure [dbo].[Modifier_CLIENT]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Modifier_CLIENT]
	@ID_CLIENT INT,@NOM_CLIENT VARCHAR(30),@Adress VARCHAR(50), @TEL_CLIENT VARCHAR(12), @FAX_CLIENT VARCHAR(50)
	
AS

BEGIN
  UPDATE[Client]
  SET       [NomClt] = @NOM_CLIENT,
			[AdressClt] = @Adress,
			[TelClt] = @TEL_CLIENT,
			[FaxClt] = @FAX_CLIENT
  WHERE [CltID] = @ID_CLIENT 
END
GO
/****** Object:  StoredProcedure [dbo].[UpdatePRODUCT]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdatePRODUCT]
	@ID_prd INT, @NOM_PRODUIT VARCHAR(40), @QTE_STOCK INT, @PRIX VARCHAR(25),@ID_FOUR INT
AS

update [Produit] set [NomProduit] = @NOM_PRODUIT,
					 [QuantiteEnStock] = @QTE_STOCK,
					 [PrixUnitaire] = @PRIX,
					 [IDFournisseur] = @ID_FOUR
					 WHERE	[IDProduit] = @ID_prd
GO
/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 9/13/2020 1:42:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateUser]
	@EmailUser VARCHAR(50),@NOMUser VARCHAR(50), @PRNUser VARCHAR(50),
    @PASS VARCHAR(50),@dateNaiss date, @imgUser VARCHAR(50)
AS
UPDATE [utilisateur] set
			
            [userName] = @NOMUser
           ,[prenomUser] = @PRNUser
		   ,[password] = @PASS
		   ,[dateNaiss] = @dateNaiss
           ,[imageUser] = @imgUser
		   WHERE [Email]= @EmailUser
GO
