﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace appProjetDeStage
{
    public partial class Produit : UserControl
    {
        public Produit()
        {
            InitializeComponent();
        }
        private static Produit userProd;
        public static Produit Instanc
        {
            get
            {
                if (userProd == null)
                {
                    userProd = new Produit();
                }
                return userProd;
            }
        }
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        private void Supprimer()
        {
            DataTable dt = new DataTable();
            int select = 0;
            for (int i = 0; i < dgvProduit.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvProduit.Rows[i].Cells[0].Value) == true)
                {
                    select++;
                }
            }
            if (select == 0)
            {
                MessageBox.Show("Aucun Produit n'a été sélectionné!", "Suppresion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult r = MessageBox.Show("voulez Vous vraiment supprimer?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {

                    for (int i = 0; i < dgvProduit.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dgvProduit.Rows[i].Cells[0].Value) == true)
                        {
                            string req = "delete from Produit where IDProduit =" +
                            dgvProduit.Rows[i].Cells["IDProduit"].Value.ToString();
                            SqlCommand cmd = new SqlCommand(req, cn);
                            SqlDataAdapter daprd = new SqlDataAdapter(cmd);

                            daprd.Fill(dt);
                            SqlCommandBuilder cb = new SqlCommandBuilder(daprd);
                            daprd.Update(dt);
                        }
                    }

                    ListProd();
                    MessageBox.Show("Produit Supprimer avec succées.", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("Suppresion est Annulé", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        public void ListProd()
        {
           
            SqlDataAdapter da;
            DataTable dt;
            da = new SqlDataAdapter("select * from Produit", cn);
            dt = new DataTable();
            da.Fill(dt);
            dgvProduit.DataSource = dt;
            if (dgvProduit.Columns[0].HeaderText != "Supprimer")
            {
                DataGridViewCheckBoxColumn chek = new DataGridViewCheckBoxColumn();
                chek.HeaderText = "Supprimer";
                dgvProduit.Columns.Insert(0, chek);
            }
        }

        AddProduct addPrd;
        private void Produit_Load(object sender, EventArgs e)
        {
            addPrd = new AddProduct();
            ListProd();
        }

      
   
        private void btAjouter_Click(object sender, EventArgs e)
        {
         
          
        }

        private void dgvProduit_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private void btSupp_Click_1(object sender, EventArgs e)
        {
            Supprimer();
        }

        private void btModd_Click(object sender, EventArgs e)
        {
            modifierPrd modifierprd = new modifierPrd();
            modifierprd.Idprd = int.Parse(dgvProduit.CurrentRow.Cells[1].Value.ToString());
            modifierprd.Show();

            ////change user controle to a forme and copy code or add other user controf for btModifier
            //addPrd.ope = 1;
            //modifierPrd modifierprd = new modifierPrd();
            //if (!Home.Instanc.pnlContainer.Controls.ContainsKey("modifierPrd"))
            //{
            //    modifierprd.Idprd = int.Parse(dgvProduit.CurrentRow.Cells[1].Value.ToString());
            //    Home.Instanc.pnlContainer.Controls.Add(modifierprd);
            //    modifierprd.Dock = DockStyle.Fill;
            //    Home.Instanc.BringToFront();
            //}
            //else
            //{
            //    modifierprd.Idprd = int.Parse(dgvProduit.CurrentRow.Cells[1].Value.ToString());
            //    Home.Instanc.pnlContainer.Controls.Add(modifierprd);
            //  //  AddProduct.Instanc.ModifierLoad();
            //}
            //Home.Instanc.pnlContainer.Controls["modifierPrd"].BringToFront();
            //Home.Instanc.backbutton.Visible = true;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            AddProduct addPrd = new AddProduct();
            addPrd.ope = 0;
            if (!Home.Instanc.pnlContainer.Controls.ContainsKey("AddProduct"))
            {
                addPrd.Dock = DockStyle.Fill;
                Home.Instanc.pnlContainer.Controls.Add(addPrd);
                Home.Instanc.BringToFront();
            }
            Home.Instanc.pnlContainer.Controls["AddProduct"].BringToFront();
            Home.Instanc.backbutton.Visible = true;
            Home.prevPanel = 0;
        }

    
    }
}
