﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appProjetDeStage
{
    public partial class MIseAjourClient : Form
    {
        public int ope { get; set; }
        public int IdClt { get; set; }
        public MIseAjourClient()
        {
            InitializeComponent();
        }
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        SqlDataAdapter daClt;
        DataTable dtClt;
        SqlCommandBuilder cbClt;

        private void Ajouter()
        {
            daClt = new SqlDataAdapter("SELECT CltID FROM Client", cn);
             dtClt= new DataTable();
            daClt.Fill(dtClt);
            bool tr = false;
            for (int i = 0; i < dtClt.Rows.Count; i++)
            {
                if (dtClt.Rows[i]["CltID"].ToString() == txtIDClt.Text)
                    tr = true;
            }
            if (tr)
                MessageBox.Show("Ce Client Existe déjà !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                daClt = new SqlDataAdapter("select max(CltID)+1 from Client", cn);
                dtClt = new DataTable();
                daClt.Fill(dtClt);
                int newId = int.Parse(dtClt.Rows[0][0].ToString());
                SqlCommand cmd = new SqlCommand("Ajouter_CLIENTS", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_CLIENT", newId);
                cmd.Parameters.AddWithValue("@NOM_CLIENT", txtNomClt.Text);
                cmd.Parameters.AddWithValue("@Adress", txtAdrClt.Text);
                cmd.Parameters.AddWithValue("@TEL_CLIENT", txtTelClt.Text);
                cmd.Parameters.AddWithValue("@FAX_CLIENT", txtFaxClt.Text);
        
                
                daClt = new SqlDataAdapter(cmd);
                dtClt = new DataTable();
                daClt.Fill(dtClt);
                cbClt = new SqlCommandBuilder(daClt);
                daClt.Update(dtClt);
                MessageBox.Show("Client Ajouter avec succés.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
        
        private void ModifierLoad()
        {
            //MessageBox.Show(IdClt.ToString());
            txtIDClt.Enabled = false;
            dtClt = new DataTable();
            daClt = new SqlDataAdapter("SELECT * FROM Client WHERE CltID = " + IdClt, cn);
            daClt.Fill(dtClt);
            txtIDClt.Text = dtClt.Rows[0][0].ToString();
            txtNomClt.Text = dtClt.Rows[0][1].ToString();
            txtAdrClt.Text = dtClt.Rows[0][2].ToString();
            txtTelClt.Text = dtClt.Rows[0][3].ToString();
            txtFaxClt.Text = dtClt.Rows[0][4].ToString();
        }
        private void Modifier()
        {
            button1.Text = "Modifier";
            bool tr = false;
            for (int i = 0; i < dtClt.Rows.Count; i++)
            {
                if (dtClt.Rows[i]["CltID"].ToString() == txtIDClt.Text)
                    tr = true;
            }
            if (tr)
            {
                
                SqlCommand cmd = new SqlCommand("Modifier_CLIENT", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_CLIENT", txtIDClt.Text);
                cmd.Parameters.AddWithValue("@NOM_CLIENT", txtNomClt.Text);
                cmd.Parameters.AddWithValue("@Adress", txtAdrClt.Text);
                cmd.Parameters.AddWithValue("@TEL_CLIENT", txtTelClt.Text);
                cmd.Parameters.AddWithValue("@FAX_CLIENT", txtFaxClt.Text);
            
               
                daClt = new SqlDataAdapter(cmd);
                dtClt = new DataTable();
                daClt.Fill(dtClt);
                cbClt = new SqlCommandBuilder(daClt);
              
            }
            else
            
               MessageBox.Show("Ce Client n'existe pas !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //id enabled=fa&lse; + auto increment
          
            //remplir les textbox avec les donnees du client avec le code IdClt


        } 
            
    
        
        
        private void MIseAjourClient_Load(object sender, EventArgs e)
        {
            switch(ope)
            {
                case 0 : button1.Text = "Ajouter";
                    break;
                case 1 : ModifierLoad();
                        break;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(button1.Text == "Ajouter")
            {
                Ajouter();
            }
            if (button1.Text == "Modifier")
            {
                Modifier();
                //code de modification d'un client utilisant la propriete IdClie
            }
        }

        private void MIseAjourClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Voulez-vous enregidtree les modifications ?", "attention", MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK)
            {
                //update database tabel 
                daClt.Update(dtClt);
               
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                //cancel modifications 
                this.DialogResult = DialogResult.Cancel;
            }


        }

        private void jImageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        Point p = new Point();
        int x, y;

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            x = Control.MousePosition.X - this.Location.X;
            y = Control.MousePosition.Y - this.Location.Y;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                p = Control.MousePosition;
                p.Y -= y;
                p.X -= x;
                this.Location = p;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
