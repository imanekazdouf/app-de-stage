﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.Windows.Forms;
using appProjetDeStage.Reports;

namespace appProjetDeStage
{
    public partial class Facture : UserControl
    {
        public Facture()
        {
            InitializeComponent();
        }

        public int operation { get; set; }
        public static int IDcmd { get; set; }
        public static int IDFour { get; set; }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
         
        }

        private void Facture_Load(object sender, EventArgs e)
        {
            if(operation == 2)
            {
                string email = Home.user.ItemArray[0].ToString();
                Facturecr fc = new Facturecr();
                fc.SetParameterValue("IDcmd", IDcmd);
                fc.SetParameterValue("email", email);
                crystalReportViewer1.ReportSource = fc;
                crystalReportViewer1.Refresh();
            }
            if(operation == 3)
            {
                bonFournisseur bf = new bonFournisseur();
                bf.SetParameterValue("idfour", IDFour);
                crystalReportViewer1.ReportSource = bf;
                crystalReportViewer1.Refresh();
            }
            
        }
    }
}
