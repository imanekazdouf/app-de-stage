﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace appProjetDeStage
{
    public partial class UpdateProduct : UserControl
    {
        public UpdateProduct()
        {
            InitializeComponent();
        }
        public int Idprd { get; set; }
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        SqlDataAdapter daFour, daModPrd;
        DataTable dtFour, dtModPrd;
        SqlCommandBuilder cbModPrd;
        private void Modifier()
        {
            btMaj.Text = "Modifier";
            bool tr = false;
            for (int i = 0; i < dtModPrd.Rows.Count; i++)
            {
                if (dtModPrd.Rows[i]["IDProduit"].ToString() == txtId.Text)
                    tr = true;
            }
            if (tr)
            {
                SqlCommand cmd = new SqlCommand("[UpdatePRODUCT]", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_prd", txtId.Text);
                cmd.Parameters.AddWithValue("@NOM_PRODUIT", txtNomProd.Text);
                cmd.Parameters.AddWithValue("@QTE_STOCK", txtquantite.Text);
                cmd.Parameters.AddWithValue("@PRIX", txtprix.Text);
                cmd.Parameters.AddWithValue("@ID_FOUR", CbxFour.SelectedValue);
                //id enabled=fa&lse; + auto increment

                daModPrd = new SqlDataAdapter(cmd);
                dtModPrd = new DataTable();
                daModPrd.Fill(dtModPrd);
                cbModPrd = new SqlCommandBuilder(daModPrd);
                if (MessageBox.Show("Voulez-vous enregidtree les modifications ?", "attention", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)

                    daModPrd.Update(dtModPrd);


            }
            else

                MessageBox.Show("Ce Client n'existe pas !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btMaj_Click(object sender, EventArgs e)
        {
            Modifier();
        }

        private void txtIDPro_TextChanged(object sender, EventArgs e)
        {

        }

        public void ModifierLoad()
        {
            txtId.Enabled = false;
            dtModPrd = new DataTable();
            MessageBox.Show(Idprd.ToString());

            daModPrd = new SqlDataAdapter("SELECT * FROM Produit WHERE IDProduit = " + Idprd, cn);
            daModPrd.Fill(dtModPrd);
            MessageBox.Show(dtModPrd.Rows.Count + " " + Idprd + " " + dtModPrd.Rows[0][3].ToString());
            txtId.Text = dtModPrd.Rows[0][0].ToString();
            txtNomProd.Text = dtModPrd.Rows[0][1].ToString();
            txtquantite.Text = dtModPrd.Rows[0][2].ToString();
            txtprix.Text = dtModPrd.Rows[0][3].ToString();
            CbxFour.Text = dtModPrd.Rows[0][4].ToString();
        }
        private void UpdateProduct_Load(object sender, EventArgs e)
        {
            daFour = new SqlDataAdapter("select * from Fournisseur", cn);
            dtFour = new DataTable();
            daFour.Fill(dtFour);

            CbxFour.DisplayMember = "SOCIETE";
            CbxFour.ValueMember = "IDFournisseur";
            CbxFour.DataSource = dtFour;
            ModifierLoad();
        }
    }
}
