﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace appProjetDeStage
{
    public partial class Commande : UserControl
    {
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        SqlDataAdapter da,daCmd;
        DataTable dt, tab, tabProd;
        public Commande()
        {
            InitializeComponent();
        }
        private static Commande usercmd;
        public static Commande Instanc
        {
            get
            {
                if (usercmd == null)
                {
                    usercmd = new Commande();
                }
                return usercmd;
            }
        }
        public int IdClt { get; set; }
        public int operation { get; set; }
       // SqlDataAdapter da;


        private double nbrPotrineT(double nbrpoutr)
        {
            double np = 0;
            foreach (DataGridViewRow dr in dgvUnit.Rows)
            {
                np += nbrpoutr;
            }
            return np;
        }
        private double pts(string surface)
        {
            return double.Parse(surface) / 10;
        }
        private double hourdis(string surface)
        {
            return double.Parse(surface) * 8.22;
        }
        private double MetreLineaire(double axe,double nbrPoutrelle)
        {
            double ML = 0;
            foreach (DataGridViewRow r in dgvUnit.Rows)
            {
                ML += axe * nbrPoutrelle;
            }
            return ML;
        }
        private double mCaree(double axe, double vide)
        {
            return axe * vide;
        }
        private double mCarreTotale()
        {
            double  Mc = 0;
            foreach(DataGridViewRow  dr in dgvUnit.Rows)
            {
               Mc += mCaree(double.Parse(dr.Cells["axe"].Value.ToString()), double.Parse(dr.Cells["vide"].Value.ToString()));
            }
            return Mc;
        }
        private void loadClient()
        {
            try
            {
                da = new SqlDataAdapter("select m.*,u.*,c.CltID from Mesure m,Unite u,Commande c where m.chambre = u.chambre and u.IDCommande = c.IDCommande and c.CltID = " + IdClt, cn);
                dt = new DataTable();
                da.Fill(dt);
                
                txtAxe.Text = dt.Rows[0]["axe"].ToString();
                txtVide.Text = dt.Rows[0]["vide"].ToString();
                txtMcarre.Text = dt.Rows[0]["MCarre"].ToString();
                txtML.Text = dt.Rows[0]["ML"].ToString();
                txtNH.Text = dt.Rows[0]["NH"].ToString();
                txtNP.Text = dt.Rows[0]["nbrPoutrelle"].ToString();
                txtPTS.Text = dt.Rows[0]["PTS"].ToString();
                txtSurface.Text = dt.Rows[0]["NPTotal"].ToString();
                dgvUnit.DataSource = dt;
            }
            catch(Exception ex)
            {
                MessageBox.Show("erreur de base  de donnée " +ex.Message);
            }
        }
        private void Commande_Load(object sender, EventArgs e)
        {
            tab = new DataTable();
            tab.Columns.Add("Chambre", typeof(int));
            tab.Columns.Add("Axe", typeof(float));
            tab.Columns.Add("Vide", typeof(float));
            tab.Columns.Add("Nombre Poutrelle", typeof(int));
            dgvUnit.DataSource = tab;
            dgvUnit.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvUnit.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            tabProd = new DataTable();
            tabProd.Columns.Add("Produit", typeof(string));
            tabProd.Columns.Add("Quantite", typeof(int));
            dgvAddProduit.DataSource = tabProd;


            //combo box produit display member nom prod value mamber id
            SqlDataAdapter da = new SqlDataAdapter("select * from Produit", cn);
            DataTable tabl = new DataTable();
            da.Fill(tabl);
            cbxProduit.DisplayMember = "NomProduit";
            cbxProduit.ValueMember = "IDProduit";
            cbxProduit.DataSource = tabl;
            groupBox1.Visible = false;
            dgvAddProduit.Visible = false;
            if(operation == 1)
            {
                loadClient();
            }
                
        }
        DataRow drw;
     
        int id;
        private void button1_Click(object sender, EventArgs e)
        {
            if(dgvUnit.Rows.Count == 0)
            {
                da = new SqlDataAdapter("select max(chambre) from Unite", cn);
                dt = new DataTable();
                da.Fill(dt);
                id = int.Parse(dt.Rows[0][0].ToString());
            }
            
            drw = tab.NewRow();
            drw[0] = id++;
            drw[1] = txtAxe.Text;
            drw[2] = txtVide.Text;
            drw[3] = float.Parse(txtVide.Text) / 0.62;
            tab.Rows.Add(drw);
            
            dgvUnit.DataSource = tab;

            
            txtMcarre.Text = mCaree(double.Parse(dgvUnit.CurrentRow.Cells["axe"].Value.ToString()), double.Parse(dgvUnit.CurrentRow.Cells["vide"].Value.ToString())).ToString();
            txtSurface.Text = mCarreTotale().ToString();
            txtNP.Text = nbrPotrineT(double.Parse(dgvUnit.Rows[0].Cells["Nombre Poutrelle"].Value.ToString())).ToString();
            txtPTS.Text = pts(txtSurface.Text).ToString();
            txtNH.Text = hourdis(txtSurface.Text).ToString();
            txtML.Text = MetreLineaire(double.Parse(dgvUnit.Rows[0].Cells["axe"].Value.ToString()), double.Parse(dgvUnit.Rows[0].Cells["Nombre Poutrelle"].Value.ToString())).ToString();
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            DataRow drP = tabProd.NewRow();
            drP[0] = cbxProduit.SelectedValue;
            drP[1] = txtQte.Text;
            tabProd.Rows.Add(drP);
            dgvAddProduit.DataSource = tabProd;
        }

        private void txtSurface_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvUnit_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMcarre.Text = mCaree(double.Parse(dgvUnit.CurrentRow.Cells["axe"].Value.ToString()),
                double.Parse(dgvUnit.CurrentRow.Cells["vide"].Value.ToString())).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(btAutre.Text == "Autre")
            {
                groupBox1.Visible = true;
                dgvAddProduit.Visible = true;
                btAutre.Text = "Hide";
            }else
            {
                if (MessageBox.Show("voulez vous enregisrer cette ajout?", "confirmation", MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK)
                {
                    daCmd = new SqlDataAdapter("insert into DetailCommande(IDProduit,QuantiteCmd) values (" + dgvAddProduit.Rows[0].Cells["Produit"].Value.ToString() + "," + dgvAddProduit.Rows[0].Cells["Quantite"].Value.ToString() + ")", cn);
                    DataTable dtCmd = new DataTable();
                    daCmd.Fill(dtCmd);
                    SqlCommandBuilder cbDTCmd = new SqlCommandBuilder(daCmd);
                    daCmd.Update(dtCmd);
                }
                groupBox1.Visible = false;
                dgvAddProduit.Visible = false;
                btAutre.Text = "Autre";
            }

        }

        private void dgvUnit_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }


        private double PrixTotal()
        {
            double mc = mCarreTotale();
            double prixT = double.Parse(((mc * 85 ) + (Convert.ToDouble(txtQte.Text) * 40) + Convert.ToDouble(txtTrans.Text)).ToString());

            return prixT;
        }
    
        internal void saveCommande()
        {
            //code d'enregistrement d'une commande
            string req = "INSERT INTO Commande values IDCommande = " + id + 1 + " CltID = " + IdClt +
                " DateCommande = '" + DateTime.Now.ToShortDateString() + "' PrixTotal = " + PrixTotal();
            //save req to database
            da = new SqlDataAdapter(req, cn);
            dt = new DataTable();
            da.Fill(dt);
            SqlCommandBuilder cbCmd = new SqlCommandBuilder(da);
            da.Update(dt);
            //detail commande req + save to database => les produit ajoutee dans nla grille 2
            daCmd = new SqlDataAdapter("insert into DetailCommande values (" + dgvAddProduit.Rows[0].Cells["Produit"].Value.ToString() + "," + dgvAddProduit.Rows[0].Cells["Quantite"].Value.ToString()+")",cn);
            DataTable dtCmd = new DataTable();
            daCmd.Fill(dtCmd);
            SqlCommandBuilder cbDTCmd = new SqlCommandBuilder(daCmd);
            daCmd.Update(dtCmd);
            //insert into Unit + save to database boucle
            for(int i = 0; i< dgvUnit.Rows.Count;i++)
            {
                SqlDataAdapter daUnit = new SqlDataAdapter("insert into Unite values (" + dgvUnit.Rows[i].Cells["Chambre"].Value.ToString() + "," +dgvUnit.Rows[i].Cells["axe"].Value.ToString() + "," + dgvUnit.Rows[i].Cells["vide"].Value.ToString() +")", cn);
                DataTable dtUnit = new DataTable();
                daUnit.Fill(dtUnit);
                SqlCommandBuilder cbUnit = new SqlCommandBuilder(daUnit);
                daUnit.Update(dtUnit);
            }

            //insert into Plan + save to database
           SqlDataAdapter daPlan = new SqlDataAdapter("insert into Plan(date,chambre)values('"+ DateTime.Now.ToShortDateString()+"',"+ dgvUnit.Rows[0].Cells["Chambre"].Value.ToString()+")", cn);
            DataTable dtPlan = new DataTable();
            daPlan.Fill(dtPlan);
            SqlCommandBuilder cbPlan = new SqlCommandBuilder(daPlan);
            daPlan.Update(dtPlan);
        }
    }
}
