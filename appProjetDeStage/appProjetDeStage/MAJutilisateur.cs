﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Drawing.Imaging;

namespace appProjetDeStage
{
    public partial class MAJutilisateur : UserControl
    {
        public MAJutilisateur()
        {
            InitializeComponent();
        }
        private static MAJutilisateur userMaj;
        public static MAJutilisateur Instanc
        {
            get
            {
                if (userMaj == null)
                {
                    userMaj = new MAJutilisateur();
                }
                return userMaj;
            }
        }
       
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        SqlDataAdapter da;
        DataTable dt;
       

        SqlCommandBuilder cb;
        int ind;
        private string strFile;
        private string iName;
        private void enter(Button b)
        {
            b.BackColor = Color.FromArgb(21, 33, 45);
            b.ForeColor = Color.FromArgb(229, 126, 46);
        }
        private void leave(Button b)
        {
            b.BackColor = Color.Transparent;
            b.ForeColor = Color.LightGray;
        }
        void afficher(int i)
        {
            //byte[] stringArray = Encoding.UTF8.GetBytes("aaa");
            txtEmail.Text = dt.Rows[i][0].ToString();
            txtNomUser.Text = dt.Rows[i][1].ToString();
            txtprenm.Text = dt.Rows[i][2].ToString();
            txtPass.Text = dt.Rows[i][3].ToString();
            dateTimePicker1.Text = dt.Rows[i][4].ToString();
            //MessageBox.Show(dt.Rows[i]["imageUser"].ToString());
            pictureBoxUser.BackgroundImage = Image.FromFile(Path.Combine(appPath, dt.Rows[i]["imageUser"].ToString()));
            iName = dt.Rows[i]["imageUser"].ToString();
          //  Byte[] data = (Byte[]) dt.Rows[i]["imageUser"];
          //MemoryStream mem = new MemoryStream(data);
          //mem.Seek(0, SeekOrigin.Begin);
          //  pictureBoxUser.Image = Image.FromStream(mem);

        }
        void popTable()
        {
            da = new SqlDataAdapter("select * from utilisateur", cn);
            dt = new DataTable();
            da.Fill(dt);
        }
        private void MAJutilisateur_Load(object sender, EventArgs e)
        {
            popTable();
            ind = 0;
            if (dt.Rows.Count > 0)
                afficher(ind);

            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
  
        }

        private void pictureBox4_MouseEnter(object sender, EventArgs e)
        {
            pictureBox4.Image = Properties.Resources.first_track__1_;
            
         

        }

        private void pictureBox4_MouseLeave(object sender, EventArgs e)
        {

            pictureBox4.Image = Properties.Resources.first_track;
        }

        private void pictureBox6_MouseEnter(object sender, EventArgs e)
        {
            pictureBox6.Image = Properties.Resources.left_arrow__1_;
        }

        private void pictureBox6_MouseLeave(object sender, EventArgs e)
        {
            pictureBox6.Image = Properties.Resources.left_arrow;
        }

        private void pictureBox7_MouseEnter(object sender, EventArgs e)
        {
            pictureBox7.Image = Properties.Resources.right_arrow__1_;
        }

        private void pictureBox7_MouseLeave(object sender, EventArgs e)
        {
            pictureBox7.Image = Properties.Resources.right_arrow;
        }

        private void pictureBox8_MouseEnter(object sender, EventArgs e)
        {
            pictureBox8.Image = Properties.Resources.last_track__1_;
        }

        private void pictureBox8_MouseLeave(object sender, EventArgs e)
        {
            pictureBox8.Image = Properties.Resources.last_track;
        }
        string appPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\Resources\userImages";
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Directory.Exists(appPath) == false)
                {
                    Directory.CreateDirectory(appPath);
                }
                else
                {
                    File.Copy(strFile, Path.Combine(appPath, iName));
                    SqlCommand cmd = new SqlCommand("[AddUser]", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@NomUser", txtNomUser.Text);
                    cmd.Parameters.AddWithValue("@PrenmUser", txtprenm.Text);
                    cmd.Parameters.AddWithValue("@PASSUser", txtPass.Text);
                    cmd.Parameters.AddWithValue("@dateNaiss",dateTimePicker1.Value);
                    cmd.Parameters.AddWithValue("@imgUser", iName);

                    da = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    da.Fill(dt);
                    cb = new SqlCommandBuilder(da);
                    da.Update(dt);
                    popTable();
                    MessageBox.Show("Utilisateur ajoutée avec succés.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur : " + ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            enter(btAjouter);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            leave(btAjouter);
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            enter(btModifier);
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            leave(btModifier);
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            enter(btSupp);
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            leave(btSupp);
        }

        private void txtNomUser_TextChanged(object sender, EventArgs e)
        {
            pictureBox2.Image = Properties.Resources.login__2_;
            panel4.BackColor = Color.FromArgb(229, 131, 46);
            txtNomUser.ForeColor = Color.FromArgb(229, 131, 46);

            pictureBoxPrenm.Image = Properties.Resources.login;
            panel5.BackColor = Color.WhiteSmoke;
            txtprenm.ForeColor = Color.WhiteSmoke;

            pictureBox3.Image = Properties.Resources.unlocked_padlock__1_;
            panel3.ForeColor = Color.WhiteSmoke;
            txtPass.ForeColor = Color.WhiteSmoke;


            pictureBoxEmail.Image = Properties.Resources.login;
            panelEmail.BackColor = Color.WhiteSmoke;
            txtEmail.ForeColor = Color.WhiteSmoke;
        }

        private void txtprenm_TextChanged(object sender, EventArgs e)
        {
            pictureBoxPrenm.Image = Properties.Resources.login__2_;
            panel5.BackColor = Color.FromArgb(229, 131, 46);
            txtprenm.ForeColor = Color.FromArgb(229, 131, 46);

            pictureBox2.Image = Properties.Resources.login;
            panel4.BackColor = Color.WhiteSmoke;
            txtNomUser.ForeColor = Color.WhiteSmoke;

            pictureBox3.Image = Properties.Resources.unlocked_padlock__1_;
            panel3.BackColor = Color.WhiteSmoke;
            txtPass.ForeColor = Color.WhiteSmoke;

            pictureBoxEmail.Image = Properties.Resources.login;
            panelEmail.BackColor = Color.WhiteSmoke;
            txtEmail.ForeColor = Color.WhiteSmoke;

        }

        private void txtPass_TextChanged(object sender, EventArgs e)
        {
            pictureBox3.Image = Properties.Resources.unlocked_padlock;
            panel3.BackColor = Color.FromArgb(229, 131, 46);
            txtPass.ForeColor = Color.FromArgb(229, 131, 46);

            pictureBoxPrenm.Image = Properties.Resources.login;
            panel5.BackColor = Color.WhiteSmoke;
            txtprenm.ForeColor = Color.WhiteSmoke;

            pictureBox2.Image = Properties.Resources.login;
            panel4.BackColor = Color.WhiteSmoke;
            txtNomUser.ForeColor = Color.WhiteSmoke;


            pictureBoxEmail.Image = Properties.Resources.login;
            panelEmail.BackColor = Color.WhiteSmoke;
            txtEmail.ForeColor = Color.WhiteSmoke;
        }


        private void btparImg_Click_1(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Images(.jpg,.jpeg)|*.jpg;*jpeg|Tous |*.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    strFile = ofd.FileName;
                    iName = ofd.SafeFileName;
                    pictureBoxUser.BackgroundImage = new Bitmap(strFile);
                }
                else
                {
                    ofd.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur: " + ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //OpenFileDialog opFile = new OpenFileDialog();
            //opFile.Title = "Select a Image";
            //opFile.Filter = "jpg files (*.jpg)|*.jpg|All files (*.*)|*.*";
            //if (opFile.ShowDialog() == DialogResult.OK)
            //{
            //    try
            //    {
            //        string iName = opFile.FileName;
            //        string filepath = "~/images/" + opFile.FileName;
            //        File.Copy(iName, Path.Combine("~\\ProImages\\", Path.GetFileName(iName)));
            //        pictureBoxUser.Image = new Bitmap(opFile.OpenFile());
            //    }
            //    catch (Exception exp)
            //    {
            //        MessageBox.Show("Unable to open file " + exp.Message);
            //    }
            //}
            //else
            //{
            //    opFile.Dispose();
            //}
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            ind = 0;
            afficher(ind);
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            if (ind > 0)
                afficher(--ind);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            if (ind < dt.Rows.Count - 1)
                afficher(++ind);
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            ind = dt.Rows.Count - 1;
            afficher(ind);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UpdateUser]", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmailUser", txtEmail.Text);
                cmd.Parameters.AddWithValue("@NOMUser", txtNomUser.Text);
                cmd.Parameters.AddWithValue("@PRNUser", txtprenm.Text);
                cmd.Parameters.AddWithValue("@PASS", txtPass.Text);
                cmd.Parameters.AddWithValue("@dateNaiss", dateTimePicker1.Value);
               
                cmd.Parameters.AddWithValue("@imgUser", iName);

                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                cb = new SqlCommandBuilder(da);
                da.Update(dt);

                popTable();
                MessageBox.Show("Utilisateur Modifier avec succés.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur : " + ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
           try
            {
                da = new SqlDataAdapter("DELETE FROM utilisateur WHERE Email = '"+txtEmail.Text+"'", cn);
                dt = new DataTable();
                da.Fill(dt);
                MessageBox.Show("Utilisateur Supprimer avec succées.", "suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);
             
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBoxUser_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            pictureBoxEmail.Image = Properties.Resources.login__2_;
            panelEmail.BackColor = Color.FromArgb(229, 131, 46);
            txtEmail.ForeColor = Color.FromArgb(229, 131, 46);

            pictureBoxPrenm.Image = Properties.Resources.login;
            panel5.BackColor = Color.WhiteSmoke;
            txtprenm.ForeColor = Color.WhiteSmoke;

            pictureBox2.Image = Properties.Resources.login;
            panel4.BackColor = Color.WhiteSmoke;
            txtNomUser.ForeColor = Color.WhiteSmoke;

            pictureBox3.Image = Properties.Resources.unlocked_padlock__1_;
            panel3.BackColor = Color.WhiteSmoke;
            txtPass.ForeColor = Color.WhiteSmoke;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btparImg_MouseEnter(object sender, EventArgs e)
        {
            enter(btparImg);
        }

        private void btparImg_MouseLeave(object sender, EventArgs e)
        {
            leave(btparImg);
        }
    }
}
