﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appProjetDeStage
{
    public partial class Home : Form
    {
        
        public Home()
        {
            InitializeComponent();
        }
        public static DataRow user { get; set; }
        public static int prevPanel { get; set; }
        private static Home obj;
        public static Home Instanc
        {
            get
            {
                if (obj == null)
                {
                    obj = new Home();
                }
                return obj;
            }
        }
        public Panel pnlContainer
        {
            get { return homePanel; }
            set { homePanel = value; }
        }
        public Button backbutton
        {
            get { return btnBack; }
            set { btnBack = value; }
        }
        
    
        private void enter(Button b)
        {
            b.BackColor = Color.FromArgb(21, 33, 45);
            b.ForeColor = Color.FromArgb(229, 126, 46);
        }
        private void leave(Button b)
        {
            b.BackColor = Color.Transparent;
            b.ForeColor = Color.LightGray;
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            backbutton.Visible = false;
            obj = this;
        }

     
        Point p = new Point();
        int x, y;
       

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {

            x = Control.MousePosition.X - this.Location.X;
            y = Control.MousePosition.Y - this.Location.Y;
        }

      

        private void headerPanel_MouseDown(object sender, MouseEventArgs e)
        {
            x = Control.MousePosition.X - this.Location.X;
            y = Control.MousePosition.Y - this.Location.Y;
        }

        private void headerPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                p = Control.MousePosition;
                p.Y -= y;
                p.X -= x;
                this.Location = p;
            }
        }

        private void logoPanel_MouseDown(object sender, MouseEventArgs e)
        {
            x = Control.MousePosition.X - this.Location.X;
            y = Control.MousePosition.Y - this.Location.Y;
        }

        private void logoPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                p = Control.MousePosition;
                p.Y -= y;
                p.X -= x;
                this.Location = p;
            }
        }

  

        private void jImageButtonMinimise_MouseEnter(object sender, EventArgs e)
        {
            jImageButtonMinimise.BackColor = Color.LightGray;

        }

        private void jImageButtonMinimise_MouseLeave(object sender, EventArgs e)
        {
            jImageButtonMinimise.BackColor = Color.White;
        }

     
        private void jImageButton2_Click_1(object sender, EventArgs e)
        {
          
                   if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;

            }
            else
            {
                this.WindowState = FormWindowState.Maximized;

            }
        }

        private void jImageButton2_MouseEnter(object sender, EventArgs e)
        {
            jImageButton2.BackColor = Color.LightGray;
        }

        private void jImageButton2_MouseLeave(object sender, EventArgs e)
        {
            jImageButton2.BackColor = Color.White;
        }

        private void jImageButton1_MouseEnter(object sender, EventArgs e)
        {
            jImageButton1.BackColor = Color.FromArgb(229, 126, 46);
        }

        private void jImageButton1_MouseLeave(object sender, EventArgs e)
        {
            jImageButton1.BackColor = Color.White;
        }

        private void jImageButtonMinimise_Click(object sender, EventArgs e)
        {

            if (this.WindowState != FormWindowState.Minimized)
                this.WindowState = FormWindowState.Minimized;
        }

        private void jImageButton1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btProd_MouseEnter(object sender, EventArgs e)
        {
            enter(btProd);
        }

        private void btProd_MouseLeave(object sender, EventArgs e)
        {
            leave(btProd);
        }

       

        private void btnCmd_Click(object sender, EventArgs e)
        {
            prevPanel = 1;
            if (!homePanel.Controls.Contains(ListeCommande.Instanc))
            {
                homePanel.Controls.Add(ListeCommande.Instanc);
                ListeCommande.Instanc.Dock = DockStyle.Fill;
                ListeCommande.Instanc.BringToFront();
            }
            else
            {
                ListeCommande.Instanc.ListCmd();
                ListeCommande.Instanc.BringToFront();
            }

        }

        private void btnCmd_MouseEnter(object sender, EventArgs e)
        {
            enter(btnCmd);
        }

        private void btnCmd_MouseLeave(object sender, EventArgs e)
        {
            leave(btnCmd);
        }

        private void btFour_MouseEnter(object sender, EventArgs e)
        {
            enter(btFour);
        }

        private void btFour_MouseLeave(object sender, EventArgs e)
        {
            leave(btFour);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            enter(button1);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            leave(button1);
        }

        private void btnClient_MouseEnter(object sender, EventArgs e)
        {
            enter(btnClient);
        }

        private void btnClient_MouseLeave(object sender, EventArgs e)
        {
            leave(btnClient);
        }

        private void btnClient_Click(object sender, EventArgs e)
        {
            if (!homePanel.Controls.Contains(Clients.Instanc))
            {
                homePanel.Controls.Add(Clients.Instanc);
                Clients.Instanc.Dock = DockStyle.Fill;
                Clients.Instanc.BringToFront();
            }
            else
            {
                Clients.Instanc.anfoClient();
                Clients.Instanc.BringToFront();
            }
        }

        private void btProd_Click(object sender, EventArgs e)
        {
            prevPanel = 0;
            if (!homePanel.Controls.Contains(Produit.Instanc))
            {
                homePanel.Controls.Add(Produit.Instanc);
                Produit.Instanc.Dock = DockStyle.Fill;
                Produit.Instanc.ListProd();
                Produit.Instanc.BringToFront();
            }
            else
            {
                Produit.Instanc.ListProd();
                Produit.Instanc.BringToFront();
            }
        }

        private void btFour_Click(object sender, EventArgs e)
        {
            if (!homePanel.Controls.Contains(Fournisseur.Instanc))
            {
                homePanel.Controls.Add(Fournisseur.Instanc);
                Fournisseur.Instanc.Dock = DockStyle.Fill;
                Fournisseur.Instanc.Listfourn();
                Fournisseur.Instanc.BringToFront();
            }
            else
            {
                Fournisseur.Instanc.Listfourn();
                Fournisseur.Instanc.BringToFront();
            }
        }

        private void homePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            string operation;
            if (prevPanel == 0)
            {
                operation = "le produit";
                homePanel.Controls["Produit"].BringToFront();
            }
            else if (prevPanel == 1)
            {

                operation = "la commande";
                homePanel.Controls["ListeCommande"].BringToFront();
            }
            else operation = "0";

            btnBack.Visible = false;

            if (operation != "0")
            {
                if (MessageBox.Show("voulez-vous enregistrer " + operation + " ?", "attention", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    Commande.Instanc.saveCommande();
                }
            }
            else
            {
                if(prevPanel == 2)
                    homePanel.Controls["ListeCommande"].BringToFront();
                if(prevPanel == 3)
                    homePanel.Controls["Fournisseur"].BringToFront();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!homePanel.Controls.Contains(MAJutilisateur.Instanc))
            {
                homePanel.Controls.Add(MAJutilisateur.Instanc);
                MAJutilisateur.Instanc.Dock = DockStyle.Fill;
                MAJutilisateur.Instanc.BringToFront();
            }
            else
            {
                // MAJutilisateur.Instanc.anfoClient();
                MAJutilisateur.Instanc.BringToFront();
            }
        }

        private void headerPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                p = Control.MousePosition;
                p.Y -= y;
                p.X -= x;
                this.Location = p;
            }
        }
    }
}
