﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace appProjetDeStage
{
    public partial class Fournisseur : UserControl
    {
        public Fournisseur()
        {
            InitializeComponent();
        }
        private static Fournisseur userFour;
        public static Fournisseur Instanc
        {
            get
            {
                if (userFour == null)
                {
                    userFour = new Fournisseur();
                }
                return userFour;
            }
        }
        public void Listfourn()
        {
            SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
            SqlDataAdapter da;
            DataTable dt;
            da = new SqlDataAdapter("select * from Fournisseur", cn);
            dt = new DataTable();
            da.Fill(dt);
            dgvFourn.DataSource = dt;
        }
        private void Fournisseur_Load(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!Home.Instanc.pnlContainer.Controls.ContainsKey("facture"))
            {
                Facture factr = new Facture();
                factr.operation = 3;
                factr.Dock = DockStyle.Fill;
                Home.Instanc.pnlContainer.Controls.Add(factr);
                Home.Instanc.BringToFront();
            }

            Facture.IDFour = int.Parse(dgvFourn.CurrentRow.Cells[0].Value.ToString());
            Home.Instanc.pnlContainer.Controls["facture"].BringToFront();
            Home.Instanc.backbutton.Visible = true;
            Home.prevPanel = 3;
        }
    }
}
