﻿namespace appProjetDeStage
{
    partial class MAJutilisateur
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MAJutilisateur));
            this.btAjouter = new System.Windows.Forms.Button();
            this.btparImg = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtprenm = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtNomUser = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.btModifier = new System.Windows.Forms.Button();
            this.btSupp = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPrenm = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxUser = new System.Windows.Forms.PictureBox();
            this.pictureBoxEmail = new System.Windows.Forms.PictureBox();
            this.panelEmail = new System.Windows.Forms.Panel();
            this.txtEmail = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrenm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmail)).BeginInit();
            this.SuspendLayout();
            // 
            // btAjouter
            // 
            this.btAjouter.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btAjouter.BackColor = System.Drawing.Color.Transparent;
            this.btAjouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btAjouter.FlatAppearance.BorderSize = 0;
            this.btAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAjouter.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAjouter.ForeColor = System.Drawing.Color.White;
            this.btAjouter.Location = new System.Drawing.Point(55, 428);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(216, 37);
            this.btAjouter.TabIndex = 89;
            this.btAjouter.Text = "Ajouter";
            this.btAjouter.UseVisualStyleBackColor = false;
            this.btAjouter.Click += new System.EventHandler(this.button1_Click);
            this.btAjouter.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.btAjouter.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // btparImg
            // 
            this.btparImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btparImg.BackColor = System.Drawing.Color.Transparent;
            this.btparImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btparImg.FlatAppearance.BorderSize = 0;
            this.btparImg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btparImg.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btparImg.ForeColor = System.Drawing.Color.White;
            this.btparImg.Location = new System.Drawing.Point(541, 346);
            this.btparImg.Name = "btparImg";
            this.btparImg.Size = new System.Drawing.Size(221, 37);
            this.btparImg.TabIndex = 88;
            this.btparImg.Text = "Parcourir ...";
            this.btparImg.UseVisualStyleBackColor = false;
            this.btparImg.Click += new System.EventHandler(this.btparImg_Click_1);
            this.btparImg.MouseEnter += new System.EventHandler(this.btparImg_MouseEnter);
            this.btparImg.MouseLeave += new System.EventHandler(this.btparImg_MouseLeave);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel5.Location = new System.Drawing.Point(53, 242);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(416, 1);
            this.panel5.TabIndex = 85;
            // 
            // txtprenm
            // 
            this.txtprenm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.txtprenm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtprenm.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprenm.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtprenm.Location = new System.Drawing.Point(90, 217);
            this.txtprenm.Name = "txtprenm";
            this.txtprenm.Size = new System.Drawing.Size(365, 19);
            this.txtprenm.TabIndex = 84;
            this.txtprenm.Text = "Prenom Utilisateur";
            this.txtprenm.TextChanged += new System.EventHandler(this.txtprenm_TextChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel4.Location = new System.Drawing.Point(53, 180);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(416, 1);
            this.panel4.TabIndex = 82;
            // 
            // txtNomUser
            // 
            this.txtNomUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.txtNomUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNomUser.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomUser.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtNomUser.Location = new System.Drawing.Point(90, 155);
            this.txtNomUser.Name = "txtNomUser";
            this.txtNomUser.Size = new System.Drawing.Size(365, 19);
            this.txtNomUser.TabIndex = 81;
            this.txtNomUser.Text = "Nom Utilisateur";
            this.txtNomUser.TextChanged += new System.EventHandler(this.txtNomUser_TextChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Location = new System.Drawing.Point(53, 307);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(416, 1);
            this.panel3.TabIndex = 79;
            // 
            // txtPass
            // 
            this.txtPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPass.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPass.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtPass.Location = new System.Drawing.Point(90, 282);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(365, 19);
            this.txtPass.TabIndex = 78;
            this.txtPass.Text = "Mot de passe";
            this.txtPass.UseSystemPasswordChar = true;
            this.txtPass.TextChanged += new System.EventHandler(this.txtPass_TextChanged);
            // 
            // btModifier
            // 
            this.btModifier.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btModifier.BackColor = System.Drawing.Color.Transparent;
            this.btModifier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btModifier.FlatAppearance.BorderSize = 0;
            this.btModifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btModifier.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btModifier.ForeColor = System.Drawing.Color.White;
            this.btModifier.Location = new System.Drawing.Point(277, 428);
            this.btModifier.Name = "btModifier";
            this.btModifier.Size = new System.Drawing.Size(216, 37);
            this.btModifier.TabIndex = 90;
            this.btModifier.Text = "Modifier";
            this.btModifier.UseVisualStyleBackColor = false;
            this.btModifier.Click += new System.EventHandler(this.button3_Click);
            this.btModifier.MouseEnter += new System.EventHandler(this.button3_MouseEnter);
            this.btModifier.MouseLeave += new System.EventHandler(this.button3_MouseLeave);
            // 
            // btSupp
            // 
            this.btSupp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btSupp.BackColor = System.Drawing.Color.Transparent;
            this.btSupp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btSupp.FlatAppearance.BorderSize = 0;
            this.btSupp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSupp.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSupp.ForeColor = System.Drawing.Color.White;
            this.btSupp.Location = new System.Drawing.Point(499, 428);
            this.btSupp.Name = "btSupp";
            this.btSupp.Size = new System.Drawing.Size(216, 37);
            this.btSupp.TabIndex = 91;
            this.btSupp.Text = "Supprimer";
            this.btSupp.UseVisualStyleBackColor = false;
            this.btSupp.Click += new System.EventHandler(this.button4_Click);
            this.btSupp.MouseEnter += new System.EventHandler(this.button4_MouseEnter);
            this.btSupp.MouseLeave += new System.EventHandler(this.button4_MouseLeave);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.CalendarForeColor = System.Drawing.SystemColors.Control;
            this.dateTimePicker1.CalendarMonthBackground = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.dateTimePicker1.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.dateTimePicker1.CalendarTitleForeColor = System.Drawing.SystemColors.Control;
            this.dateTimePicker1.CalendarTrailingForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dateTimePicker1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(187, 344);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(282, 23);
            this.dateTimePicker1.TabIndex = 92;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(52, 347);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 17);
            this.label1.TabIndex = 93;
            this.label1.Text = "Date de naissance";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::appProjetDeStage.Properties.Resources.last_track;
            this.pictureBox8.Location = new System.Drawing.Point(347, 31);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(35, 33);
            this.pictureBox8.TabIndex = 100;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            this.pictureBox8.MouseEnter += new System.EventHandler(this.pictureBox8_MouseEnter);
            this.pictureBox8.MouseLeave += new System.EventHandler(this.pictureBox8_MouseLeave);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::appProjetDeStage.Properties.Resources.right_arrow;
            this.pictureBox7.Location = new System.Drawing.Point(265, 31);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(29, 33);
            this.pictureBox7.TabIndex = 99;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureBox7_Click);
            this.pictureBox7.MouseEnter += new System.EventHandler(this.pictureBox7_MouseEnter);
            this.pictureBox7.MouseLeave += new System.EventHandler(this.pictureBox7_MouseLeave);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::appProjetDeStage.Properties.Resources.left_arrow;
            this.pictureBox6.Location = new System.Drawing.Point(176, 31);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(34, 33);
            this.pictureBox6.TabIndex = 98;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            this.pictureBox6.MouseEnter += new System.EventHandler(this.pictureBox6_MouseEnter);
            this.pictureBox6.MouseLeave += new System.EventHandler(this.pictureBox6_MouseLeave);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::appProjetDeStage.Properties.Resources.first_track;
            this.pictureBox4.Location = new System.Drawing.Point(90, 31);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(36, 33);
            this.pictureBox4.TabIndex = 97;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            this.pictureBox4.MouseEnter += new System.EventHandler(this.pictureBox4_MouseEnter);
            this.pictureBox4.MouseLeave += new System.EventHandler(this.pictureBox4_MouseLeave);
            // 
            // pictureBoxPrenm
            // 
            this.pictureBoxPrenm.Image = global::appProjetDeStage.Properties.Resources.login;
            this.pictureBoxPrenm.Location = new System.Drawing.Point(53, 217);
            this.pictureBoxPrenm.Name = "pictureBoxPrenm";
            this.pictureBoxPrenm.Size = new System.Drawing.Size(19, 19);
            this.pictureBoxPrenm.TabIndex = 96;
            this.pictureBoxPrenm.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::appProjetDeStage.Properties.Resources.unlocked_padlock__1_;
            this.pictureBox3.Location = new System.Drawing.Point(55, 282);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 21);
            this.pictureBox3.TabIndex = 95;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::appProjetDeStage.Properties.Resources.login;
            this.pictureBox2.Location = new System.Drawing.Point(55, 155);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 19);
            this.pictureBox2.TabIndex = 94;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBoxUser
            // 
            this.pictureBoxUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxUser.BackgroundImage")));
            this.pictureBoxUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxUser.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxUser.ErrorImage")));
            this.pictureBoxUser.Location = new System.Drawing.Point(541, 128);
            this.pictureBoxUser.Name = "pictureBoxUser";
            this.pictureBoxUser.Size = new System.Drawing.Size(221, 212);
            this.pictureBoxUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUser.TabIndex = 87;
            this.pictureBoxUser.TabStop = false;
            this.pictureBoxUser.Click += new System.EventHandler(this.pictureBoxUser_Click);
            // 
            // pictureBoxEmail
            // 
            this.pictureBoxEmail.Image = global::appProjetDeStage.Properties.Resources.login;
            this.pictureBoxEmail.Location = new System.Drawing.Point(55, 93);
            this.pictureBoxEmail.Name = "pictureBoxEmail";
            this.pictureBoxEmail.Size = new System.Drawing.Size(19, 19);
            this.pictureBoxEmail.TabIndex = 103;
            this.pictureBoxEmail.TabStop = false;
            this.pictureBoxEmail.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panelEmail
            // 
            this.panelEmail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelEmail.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panelEmail.Location = new System.Drawing.Point(53, 118);
            this.panelEmail.Name = "panelEmail";
            this.panelEmail.Size = new System.Drawing.Size(416, 1);
            this.panelEmail.TabIndex = 102;
            this.panelEmail.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtEmail.Location = new System.Drawing.Point(90, 93);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(365, 19);
            this.txtEmail.TabIndex = 101;
            this.txtEmail.Text = "Email";
            this.txtEmail.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // MAJutilisateur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.Controls.Add(this.pictureBoxEmail);
            this.Controls.Add(this.panelEmail);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBoxPrenm);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btSupp);
            this.Controls.Add(this.btModifier);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.btparImg);
            this.Controls.Add(this.pictureBoxUser);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.txtprenm);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.txtNomUser);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.txtPass);
            this.Name = "MAJutilisateur";
            this.Size = new System.Drawing.Size(810, 569);
            this.Load += new System.EventHandler(this.MAJutilisateur_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrenm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Button btparImg;
        private System.Windows.Forms.PictureBox pictureBoxUser;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtprenm;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtNomUser;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Button btModifier;
        private System.Windows.Forms.Button btSupp;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBoxPrenm;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBoxEmail;
        private System.Windows.Forms.Panel panelEmail;
        private System.Windows.Forms.TextBox txtEmail;
    }
}
