﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace appProjetDeStage
{
    public partial class ListeCommande : UserControl
    {
        public ListeCommande()
        {
            InitializeComponent();
        }
        private static ListeCommande userLcomm;
        public static ListeCommande Instanc
        {
            get
            {
                if (userLcomm == null)
                {
                    userLcomm = new ListeCommande();
                }
                return userLcomm;
            }
        }

        public void ListCmd()
        {
            SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
            SqlDataAdapter da;
            DataTable dt;
            da = new SqlDataAdapter("select * from Commande", cn);
            dt = new DataTable();
            da.Fill(dt);
            dgvCommande.DataSource = dt;
        }
        private void ListeCommande_Load(object sender, EventArgs e)
        {
            ListCmd();


        }



        private void button3_Click_1(object sender, EventArgs e)
        {
            if (!Home.Instanc.pnlContainer.Controls.ContainsKey("Commande"))
            {
                Commande cmd = new Commande();
                cmd.Dock = DockStyle.Fill;
                cmd.operation = 0;
                
                Home.Instanc.pnlContainer.Controls.Add(cmd);
                Home.Instanc.BringToFront();
            }
            Home.Instanc.pnlContainer.Controls["Commande"].BringToFront();
            Home.Instanc.backbutton.Visible = true;
            Home.prevPanel = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (!Home.Instanc.pnlContainer.Controls.ContainsKey("facture"))
            {
                Facture fact = new Facture();
                fact.operation = 2;
                fact.Dock = DockStyle.Fill;
                Home.Instanc.pnlContainer.Controls.Add(fact);
                Home.Instanc.BringToFront();
            }
            
            Facture.IDcmd = int.Parse(dgvCommande.CurrentRow.Cells[0].Value.ToString());
            Home.Instanc.pnlContainer.Controls["facture"].BringToFront();
            Home.Instanc.pnlContainer.Controls["facture"].Refresh();
            Home.Instanc.backbutton.Visible = true;
            Home.prevPanel = 2;
        }


        private void dgvCommande_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!Home.Instanc.pnlContainer.Controls.ContainsKey("Commande"))
            {
                Commande cmd = new Commande();
                cmd.Dock = DockStyle.Fill;
                cmd.operation = 1;
                cmd.IdClt = int.Parse(dgvCommande.CurrentRow.Cells[0].Value.ToString());
                Home.Instanc.pnlContainer.Controls.Add(cmd);
                Home.Instanc.BringToFront();
            }
            Home.Instanc.pnlContainer.Controls["Commande"].BringToFront();
            Home.Instanc.backbutton.Visible = true;
        }
    }
        
}
