﻿namespace appProjetDeStage
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.sidePanel = new System.Windows.Forms.Panel();
            this.btnClient = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCmd = new System.Windows.Forms.Button();
            this.btFour = new System.Windows.Forms.Button();
            this.btProd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.logoPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.jImageButton2 = new JImageButton.JImageButton();
            this.jImageButton1 = new JImageButton.JImageButton();
            this.jImageButtonMinimise = new JImageButton.JImageButton();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.homePanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.sidePanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.logoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // sidePanel
            // 
            this.sidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.sidePanel.Controls.Add(this.btnClient);
            this.sidePanel.Controls.Add(this.button1);
            this.sidePanel.Controls.Add(this.btnCmd);
            this.sidePanel.Controls.Add(this.btFour);
            this.sidePanel.Controls.Add(this.btProd);
            this.sidePanel.Controls.Add(this.panel1);
            this.sidePanel.Controls.Add(this.logoPanel);
            this.sidePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sidePanel.Location = new System.Drawing.Point(0, 0);
            this.sidePanel.Name = "sidePanel";
            this.sidePanel.Size = new System.Drawing.Size(168, 613);
            this.sidePanel.TabIndex = 0;
            // 
            // btnClient
            // 
            this.btnClient.BackColor = System.Drawing.Color.Transparent;
            this.btnClient.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.btnClient.FlatAppearance.BorderSize = 0;
            this.btnClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClient.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClient.ForeColor = System.Drawing.Color.LightGray;
            this.btnClient.Location = new System.Drawing.Point(0, 86);
            this.btnClient.Name = "btnClient";
            this.btnClient.Size = new System.Drawing.Size(168, 33);
            this.btnClient.TabIndex = 16;
            this.btnClient.Text = "Client";
            this.btnClient.UseVisualStyleBackColor = false;
            this.btnClient.Click += new System.EventHandler(this.btnClient_Click);
            this.btnClient.MouseEnter += new System.EventHandler(this.btnClient_MouseEnter);
            this.btnClient.MouseLeave += new System.EventHandler(this.btnClient_MouseLeave);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.LightGray;
            this.button1.Location = new System.Drawing.Point(0, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 33);
            this.button1.TabIndex = 15;
            this.button1.Text = "Compte";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // btnCmd
            // 
            this.btnCmd.BackColor = System.Drawing.Color.Transparent;
            this.btnCmd.FlatAppearance.BorderSize = 0;
            this.btnCmd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCmd.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCmd.ForeColor = System.Drawing.Color.LightGray;
            this.btnCmd.Location = new System.Drawing.Point(0, 125);
            this.btnCmd.Name = "btnCmd";
            this.btnCmd.Size = new System.Drawing.Size(168, 33);
            this.btnCmd.TabIndex = 14;
            this.btnCmd.Text = "Commande";
            this.btnCmd.UseVisualStyleBackColor = false;
            this.btnCmd.Click += new System.EventHandler(this.btnCmd_Click);
            this.btnCmd.MouseEnter += new System.EventHandler(this.btnCmd_MouseEnter);
            this.btnCmd.MouseLeave += new System.EventHandler(this.btnCmd_MouseLeave);
            // 
            // btFour
            // 
            this.btFour.BackColor = System.Drawing.Color.Transparent;
            this.btFour.FlatAppearance.BorderSize = 0;
            this.btFour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFour.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFour.ForeColor = System.Drawing.Color.LightGray;
            this.btFour.Location = new System.Drawing.Point(0, 203);
            this.btFour.Name = "btFour";
            this.btFour.Size = new System.Drawing.Size(168, 33);
            this.btFour.TabIndex = 12;
            this.btFour.Text = "Fournisseur";
            this.btFour.UseVisualStyleBackColor = false;
            this.btFour.Click += new System.EventHandler(this.btFour_Click);
            this.btFour.MouseEnter += new System.EventHandler(this.btFour_MouseEnter);
            this.btFour.MouseLeave += new System.EventHandler(this.btFour_MouseLeave);
            // 
            // btProd
            // 
            this.btProd.BackColor = System.Drawing.Color.Transparent;
            this.btProd.FlatAppearance.BorderSize = 0;
            this.btProd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btProd.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btProd.ForeColor = System.Drawing.Color.LightGray;
            this.btProd.Location = new System.Drawing.Point(0, 164);
            this.btProd.Name = "btProd";
            this.btProd.Size = new System.Drawing.Size(168, 33);
            this.btProd.TabIndex = 11;
            this.btProd.Text = "Produit";
            this.btProd.UseVisualStyleBackColor = false;
            this.btProd.Click += new System.EventHandler(this.btProd_Click);
            this.btProd.MouseEnter += new System.EventHandler(this.btProd_MouseEnter);
            this.btProd.MouseLeave += new System.EventHandler(this.btProd_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(43)))), ((int)(((byte)(55)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 543);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(168, 70);
            this.panel1.TabIndex = 9;
            // 
            // logoPanel
            // 
            this.logoPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(126)))), ((int)(((byte)(46)))));
            this.logoPanel.Controls.Add(this.pictureBox1);
            this.logoPanel.Controls.Add(this.label1);
            this.logoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.logoPanel.Location = new System.Drawing.Point(0, 0);
            this.logoPanel.Name = "logoPanel";
            this.logoPanel.Size = new System.Drawing.Size(168, 44);
            this.logoPanel.TabIndex = 0;
            this.logoPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.logoPanel_MouseDown);
            this.logoPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.logoPanel_MouseMove);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::appProjetDeStage.Properties.Resources.hhh_jpg1;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 35);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(45, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Azrou Sani";
            // 
            // headerPanel
            // 
            this.headerPanel.BackColor = System.Drawing.Color.White;
            this.headerPanel.Controls.Add(this.btnBack);
            this.headerPanel.Controls.Add(this.jImageButton2);
            this.headerPanel.Controls.Add(this.jImageButton1);
            this.headerPanel.Controls.Add(this.jImageButtonMinimise);
            this.headerPanel.Controls.Add(this.label2);
            this.headerPanel.Controls.Add(this.pictureBox3);
            this.headerPanel.Controls.Add(this.pictureBox2);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(168, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(810, 44);
            this.headerPanel.TabIndex = 1;
            this.headerPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.headerPanel_Paint);
            this.headerPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.headerPanel_MouseDown);
            this.headerPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.headerPanel_MouseMove);
            // 
            // btnBack
            // 
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Image = global::appProjetDeStage.Properties.Resources.return__3_;
            this.btnBack.Location = new System.Drawing.Point(9, 11);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(32, 24);
            this.btnBack.TabIndex = 0;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // jImageButton2
            // 
            this.jImageButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.jImageButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.jImageButton2.BackColor = System.Drawing.Color.White;
            this.jImageButton2.CausesValidation = false;
            this.jImageButton2.Cursor = System.Windows.Forms.Cursors.Default;
            this.jImageButton2.ErrorImage = ((System.Drawing.Image)(resources.GetObject("jImageButton2.ErrorImage")));
            this.jImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("jImageButton2.Image")));
            this.jImageButton2.ImageHover = null;
            this.jImageButton2.InitialImage = null;
            this.jImageButton2.Location = new System.Drawing.Point(752, 11);
            this.jImageButton2.Name = "jImageButton2";
            this.jImageButton2.Size = new System.Drawing.Size(22, 21);
            this.jImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.jImageButton2.TabIndex = 2;
            this.jImageButton2.Zoom = 4;
            this.jImageButton2.Click += new System.EventHandler(this.jImageButton2_Click_1);
            this.jImageButton2.MouseEnter += new System.EventHandler(this.jImageButton2_MouseEnter);
            this.jImageButton2.MouseLeave += new System.EventHandler(this.jImageButton2_MouseLeave);
            // 
            // jImageButton1
            // 
            this.jImageButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.jImageButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.jImageButton1.BackColor = System.Drawing.Color.White;
            this.jImageButton1.CausesValidation = false;
            this.jImageButton1.Cursor = System.Windows.Forms.Cursors.Default;
            this.jImageButton1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("jImageButton1.ErrorImage")));
            this.jImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("jImageButton1.Image")));
            this.jImageButton1.ImageHover = null;
            this.jImageButton1.InitialImage = null;
            this.jImageButton1.Location = new System.Drawing.Point(780, 11);
            this.jImageButton1.Name = "jImageButton1";
            this.jImageButton1.Size = new System.Drawing.Size(18, 21);
            this.jImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.jImageButton1.TabIndex = 2;
            this.jImageButton1.Zoom = 4;
            this.jImageButton1.Click += new System.EventHandler(this.jImageButton1_Click);
            this.jImageButton1.MouseEnter += new System.EventHandler(this.jImageButton1_MouseEnter);
            this.jImageButton1.MouseLeave += new System.EventHandler(this.jImageButton1_MouseLeave);
            // 
            // jImageButtonMinimise
            // 
            this.jImageButtonMinimise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.jImageButtonMinimise.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.jImageButtonMinimise.BackColor = System.Drawing.Color.Transparent;
            this.jImageButtonMinimise.CausesValidation = false;
            this.jImageButtonMinimise.Cursor = System.Windows.Forms.Cursors.Default;
            this.jImageButtonMinimise.ErrorImage = ((System.Drawing.Image)(resources.GetObject("jImageButtonMinimise.ErrorImage")));
            this.jImageButtonMinimise.Image = ((System.Drawing.Image)(resources.GetObject("jImageButtonMinimise.Image")));
            this.jImageButtonMinimise.ImageHover = null;
            this.jImageButtonMinimise.InitialImage = null;
            this.jImageButtonMinimise.Location = new System.Drawing.Point(728, 11);
            this.jImageButtonMinimise.Name = "jImageButtonMinimise";
            this.jImageButtonMinimise.Size = new System.Drawing.Size(20, 21);
            this.jImageButtonMinimise.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.jImageButtonMinimise.TabIndex = 11;
            this.jImageButtonMinimise.Zoom = 4;
            this.jImageButtonMinimise.Click += new System.EventHandler(this.jImageButtonMinimise_Click);
            this.jImageButtonMinimise.MouseEnter += new System.EventHandler(this.jImageButtonMinimise_MouseEnter);
            this.jImageButtonMinimise.MouseLeave += new System.EventHandler(this.jImageButtonMinimise_MouseLeave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(146, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 21);
            this.label2.TabIndex = 9;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::appProjetDeStage.Properties.Resources.server;
            this.pictureBox3.Location = new System.Drawing.Point(85, 11);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(31, 27);
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::appProjetDeStage.Properties.Resources.house__1_;
            this.pictureBox2.Location = new System.Drawing.Point(47, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 29);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // homePanel
            // 
            this.homePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homePanel.Location = new System.Drawing.Point(168, 44);
            this.homePanel.Name = "homePanel";
            this.homePanel.Size = new System.Drawing.Size(810, 569);
            this.homePanel.TabIndex = 2;
            this.homePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.homePanel_Paint);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.LightGray;
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "Realiser par :\r\n                 KAZDOUF Imane";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(978, 613);
            this.Controls.Add(this.homePanel);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.sidePanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.sidePanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.logoPanel.ResumeLayout(false);
            this.logoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel sidePanel;
        private System.Windows.Forms.Panel logoPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private JImageButton.JImageButton jImageButtonMinimise;
        private JImageButton.JImageButton jImageButton2;
        private JImageButton.JImageButton jImageButton1;
        private System.Windows.Forms.Button btFour;
        private System.Windows.Forms.Button btProd;
        private System.Windows.Forms.Panel homePanel;
        private System.Windows.Forms.Button btnCmd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnClient;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label label3;
    }
}

