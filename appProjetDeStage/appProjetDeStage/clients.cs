﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace appProjetDeStage
{
    public partial class Clients : UserControl
    {
        public Clients()
        {
            InitializeComponent();
        }

        private static Clients userclient;
        public static Clients Instanc
        {
            get
            {
                if (userclient == null)
                {
                    userclient = new Clients();
                }
                return userclient;
            }
        }
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        private void enter(Button b)
        {
            b.BackColor = Color.FromArgb(21, 33, 45);
            b.ForeColor = Color.FromArgb(229, 126, 46);
        }
        private void leave(Button b)
        {
            b.BackColor = Color.Transparent;
            b.ForeColor = Color.LightGray;
        }
   
        public void anfoClient()
        {
          
            SqlDataAdapter da;
            DataTable dt;
            da = new SqlDataAdapter("select * from Client", cn);
            dt = new DataTable();
            da.Fill(dt);
            dgvClients.DataSource = dt;
            if (dgvClients.Columns[0].HeaderText != "Supprimer")
            {
                DataGridViewCheckBoxColumn chek = new DataGridViewCheckBoxColumn();
                chek.HeaderText = "Supprimer";
                dgvClients.Columns.Insert(0, chek);
            }
             

        }
        private void clients_Load(object sender, EventArgs e)
        {
            anfoClient();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MIseAjourClient ac = new MIseAjourClient();
            ac.ope = 1;
            //MessageBox.Show(dgvClients.CurrentRow.Cells[1].Value.ToString());
            ac.IdClt = int.Parse(dgvClients.CurrentRow.Cells[1].Value.ToString());
            if (ac.ShowDialog() == DialogResult.OK)
            {
                //methode pour refraivhire les valeurs de la grille
                anfoClient();
            }
        }
        private void Supprimer()
        {
            DataTable dt = new DataTable();
            int select = 0;
            for (int i = 0; i < dgvClients.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvClients.Rows[i].Cells[0].Value) == true)
                {
                    select++;
                }
            }
            if (select == 0)
            {
                MessageBox.Show("Aucun Client n'a été sélectionné!", "Suppresion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult r = MessageBox.Show("voulez Vous vraiment supprimer?", "Confirmation",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                   
                    for (int i = 0; i < dgvClients.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dgvClients.Rows[i].Cells[0].Value) == true)
                        {
                            string req = "delete from Client where CltID =" +
                            dgvClients.Rows[i].Cells["CltID"].Value.ToString();
                            SqlCommand cmd = new SqlCommand(req, cn);
                           SqlDataAdapter daClt = new SqlDataAdapter(cmd);
                           
                            daClt.Fill(dt);
                            SqlCommandBuilder cb = new SqlCommandBuilder(daClt);
                            daClt.Update(dt);
                        }
                    }
                    
                    anfoClient();
                    MessageBox.Show("Client Supprimer avec succées.", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("Suppresion est Annulé", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private void button3_MouseEnter(object sender, EventArgs e)
        {
            enter(button3);
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            leave(button3);
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            enter(button2);
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            leave(button2);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            enter(button1);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            leave(button1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Supprimer();

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void dgvClients_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Commande cmd = new Commande();
            cmd.operation = 0;
            cmd.IdClt = int.Parse(dgvClients.CurrentRow.Cells[0].Value.ToString());
           
            cmd.Show();
     

        }

        private void jGradientPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            MIseAjourClient ac = new MIseAjourClient();
            ac.ope = 0;
            if (ac.ShowDialog() == DialogResult.OK)
            {
                anfoClient();
            }
        }

        private void jGradientPanel2_Paint_1(object sender, PaintEventArgs e)
        {

        }
    }
}
