﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace appProjetDeStage
{
    public partial class AddProduct : UserControl
    {
        public AddProduct()
        {
            InitializeComponent();
        }
        public int Idprd { get; set; }
        public int ope { get; set; }

        private static AddProduct userAddProd;
        public static AddProduct Instanc
        {
            get
            {
                if (userAddProd == null)
                {
                    userAddProd = new AddProduct();
                }
                return userAddProd;
            }
        }

        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        private SqlDataAdapter daFour, daAddPr, daModPrd;
        private DataTable dtFour, dtAddPr, dtModPrd;
        private SqlCommandBuilder cbAddPr,cbModPrd;
        private void Modifier()
        {
            btMaj.Text = "Modifier";
            bool tr = false;
            for (int i = 0; i < dtModPrd.Rows.Count; i++)
            {
                if (dtModPrd.Rows[i]["IDProduit"].ToString() == txtIDPro.Text)
                    tr = true;
            }
            if (tr)
            {
                SqlCommand cmd = new SqlCommand("[UpdatePRODUCT]", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_prd", txtIDPro.Text);
                cmd.Parameters.AddWithValue("@NOM_PRODUIT", txtNomProd.Text);
                cmd.Parameters.AddWithValue("@QTE_STOCK", txtquantite.Text);
                cmd.Parameters.AddWithValue("@PRIX", txtprix.Text);
                cmd.Parameters.AddWithValue("@ID_FOUR", CbxFour.SelectedValue);
                //id enabled=fa&lse; + auto increment

                daModPrd = new SqlDataAdapter(cmd);
                dtModPrd = new DataTable();
                daModPrd.Fill(dtModPrd);
                cbModPrd = new SqlCommandBuilder(daModPrd);
                if (MessageBox.Show("Voulez-vous enregidtree les modifications ?", "attention", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)

                    daModPrd.Update(dtModPrd);


            }
            else

                MessageBox.Show("Ce Client n'existe pas !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btMaj_Click_1(object sender, EventArgs e)
        {
            //if (btMaj.Text == "Ajouter")
            //{
                Ajouter();
            //}
            //if (btMaj.Text == "Modifier")
            //{
            //    Modifier();

            //}
        }

        private void btMaj_Click(object sender, EventArgs e)
        {
          
        }

        public void ModifierLoad()
        {
            txtIDPro.Enabled = false;
            dtModPrd = new DataTable();
            MessageBox.Show(Idprd.ToString());
            
            daModPrd = new SqlDataAdapter("SELECT * FROM Produit WHERE IDProduit = " + Idprd, cn);
            daModPrd.Fill(dtModPrd);
            MessageBox.Show(dtModPrd.Rows.Count + " " + Idprd + " " + dtModPrd.Rows[0][3].ToString());
            txtIDPro.Text = dtModPrd.Rows[0][0].ToString();
            txtNomProd.Text = dtModPrd.Rows[0][1].ToString();
            txtquantite.Text = dtModPrd.Rows[0][2].ToString();
            txtprix.Text = dtModPrd.Rows[0][3].ToString();
            CbxFour.Text = dtModPrd.Rows[0][4].ToString();
        }
        private void Ajouter()
        {

            try
            {

                SqlDataAdapter daPrd = new SqlDataAdapter("SELECT NomProduit FROM Produit", cn);
                DataTable DtPrds = new DataTable();
                daPrd.Fill(DtPrds);
                bool tr = false;
                for (int i = 0; i < DtPrds.Rows.Count; i++)
                {
                    if (DtPrds.Rows[i]["NomProduit"].ToString().ToUpper() == txtNomProd.Text.ToUpper())
                        tr = true;
                }
                if (tr)
                    MessageBox.Show("Ce produit Existe déjà !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {

                    daAddPr = new SqlDataAdapter("select max(IDProduit)+1 from Produit", cn);
                    dtAddPr = new DataTable();
                    daAddPr.Fill(dtAddPr);
                    int newId = int.Parse(dtAddPr.Rows[0][0].ToString());
                    SqlCommand cmd = new SqlCommand("ADD_PRODUCT", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID_prd", newId);
                    cmd.Parameters.AddWithValue("@NOM_PRODUIT", txtNomProd.Text);
                    cmd.Parameters.AddWithValue("@QTE_STOCK", txtquantite.Text);
                    cmd.Parameters.AddWithValue("@PRIX", txtprix.Text);
                    cmd.Parameters.AddWithValue("@ID_FOUR", CbxFour.SelectedValue);
             

                    daAddPr = new SqlDataAdapter(cmd);
                    dtAddPr = new DataTable();
                    daAddPr.Fill(dtAddPr);
                    cbAddPr = new SqlCommandBuilder(daAddPr);
                    daAddPr.Update(dtAddPr);
                    MessageBox.Show("Produit ajouté avec succés.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur: " + ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        
       }


        private void AddProduct_Load(object sender, EventArgs e)
        {
            daFour = new SqlDataAdapter("select * from Fournisseur", cn);
            dtFour = new DataTable();
            daFour.Fill(dtFour);

            CbxFour.DisplayMember = "SOCIETE";
            CbxFour.ValueMember = "IDFournisseur";
            CbxFour.DataSource = dtFour;
            switch (ope)
            {
                case 0:
                    btMaj.Text = "Ajouter";
                    break;
                //case 1:
                //    btMaj.Text = "Modifier";
                //    ModifierLoad();
                //    break;

            }
        }

   
    }
}
