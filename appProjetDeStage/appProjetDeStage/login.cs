﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appProjetDeStage
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        SqlDataAdapter da;
        DataTable dt;
        private void login_Load(object sender, EventArgs e)
        {
           
            da = new SqlDataAdapter("select * from utilisateur", cn);
            dt = new DataTable();
            da.Fill(dt);
        }
        Home hme = new Home();
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            pictureBox2.Image = Properties.Resources.login__2_;
            panel1.ForeColor = Color.FromArgb(229, 131, 46);
            txtUser.ForeColor = Color.FromArgb(229, 131, 46);

            pictureBox3.Image = Properties.Resources.unlocked_padlock__1_;
            panel2.ForeColor = Color.WhiteSmoke;
            txtpass.ForeColor = Color.WhiteSmoke;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
     
            pictureBox3.Image = Properties.Resources.unlocked_padlock;
            panel2.ForeColor = Color.FromArgb(229, 131, 46);
            txtpass.ForeColor = Color.FromArgb(229, 131, 46);

            pictureBox2.Image = Properties.Resources.login;
            panel1.ForeColor = Color.WhiteSmoke;
            txtUser.ForeColor = Color.WhiteSmoke;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["Email"].ToString() == txtUser.Text && dr["password"].ToString() == txtpass.Text)
                {
                    this.Hide();Home.user = dr;
                    Home frm = new Home();
                    
                    frm.Show();
                }
                else
                    return;
            }
           
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtpass.UseSystemPasswordChar = false;
            }
            else
                txtpass.UseSystemPasswordChar = true;
        }
    }
}
