﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appProjetDeStage
{
    public partial class modifierPrd : Form
    {
        public modifierPrd()
        {
            InitializeComponent();
        }
        public int Idprd { get; set; }
        SqlConnection cn = new SqlConnection("Data Source=.;Initial Catalog=Sells;Integrated Security=True");
        SqlDataAdapter daFour, daModPrd;
        DataTable dtFour, dtModPrd;
        SqlCommandBuilder cbModPrd;

        private void Modifier()
        {
            btMaj.Text = "Modifier";
            bool tr = false;
            for (int i = 0; i < dtModPrd.Rows.Count; i++)
            {
                if (dtModPrd.Rows[i]["IDProduit"].ToString() == txtIDPro.Text)
                    tr = true;
            }
            if (tr)
            {
                SqlCommand cmd = new SqlCommand("[UpdatePRODUCT]", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_prd", txtIDPro.Text);
                cmd.Parameters.AddWithValue("@NOM_PRODUIT", txtNomProd.Text);
                cmd.Parameters.AddWithValue("@QTE_STOCK", txtquantite.Text);
                cmd.Parameters.AddWithValue("@PRIX", txtprix.Text);
                cmd.Parameters.AddWithValue("@ID_FOUR", CbxFour.SelectedValue);
                //id enabled=fa&lse; + auto increment

                daModPrd = new SqlDataAdapter(cmd);
                dtModPrd = new DataTable();
                daModPrd.Fill(dtModPrd);
                cbModPrd = new SqlCommandBuilder(daModPrd);
                if (MessageBox.Show("Voulez-vous enregidtree les modifications ?", "attention", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)

                    daModPrd.Update(dtModPrd);
                   MessageBox.Show("Produit modifier avec succées.", "Modification", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);


            }
            else

                MessageBox.Show("Ce Client n'existe pas !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btMaj_Click(object sender, EventArgs e)
        {
            Modifier();
        }

        private void jImageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void ModifierLoad()
        {
            txtIDPro.Enabled = false;
            dtModPrd = new DataTable();
            dtFour = new DataTable();
           // MessageBox.Show(Idprd.ToString());

            daModPrd = new SqlDataAdapter("SELECT * FROM Produit WHERE IDProduit = " + Idprd, cn);
            daModPrd.Fill(dtModPrd);
            daFour = new SqlDataAdapter("select IDFournisseur from Fournisseur", cn);
            daFour.Fill(dtFour);
            CbxFour.DisplayMember = "IDFournisseur";
            CbxFour.ValueMember = "IDFournisseur";
            CbxFour.DataSource = dtFour;
            // MessageBox.Show(dtModPrd.Rows.Count + " " + Idprd + " " + dtModPrd.Rows[0][3].ToString());
            txtIDPro.Text = dtModPrd.Rows[0][0].ToString();
            txtNomProd.Text = dtModPrd.Rows[0][1].ToString();
            txtquantite.Text = dtModPrd.Rows[0][2].ToString();
            txtprix.Text = dtModPrd.Rows[0][3].ToString();
            CbxFour.SelectedValue = dtModPrd.Rows[0][4].ToString();
        }
        private void modifierPrd_Load(object sender, EventArgs e)
        {
            ModifierLoad();
        }
    }
}
